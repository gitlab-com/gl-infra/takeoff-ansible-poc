## Summary

A quick PoC for using Ansible instead of takeoff for GitLab deployments.
Generates an inventory from Chef and deploys to roles, much like what takeoff
does now.

### Advantages

* We frequently see ssh connection timeouts with takeoff, this gives us fine-tuned controls over retries.
* Easier to create pre and post checks for changes, avoid unnecessary restarts.
* Task retries for failures
* Built in support for apt and many modules
* Excellent support for rolling updates, much more configurable than `knife ssh ...` - https://docs.ansible.com/ansible/latest/user_guide/playbooks_delegation.html#rolling-update-batch-size
* Simple YAML syntax
* Plenty of options for integrating with slack
* If we continue to need to apply patch files it makes much more sense to use the same tool.
* This will be a very fast drop-in replacement

### Disadvantages 

* takeoff is a known quantity
* ansible is Python based

### PoC

A simple [playbook](https://gitlab.com/gitlab-com/gl-infra/takeoff-ansible-poc/blob/master/deploy.yml) that demonstrates the deploy process of takeoff in a very
small amount of YAML:

- Stop chef
- Update the version role in Chef for the environment we are deploying to
- Deploy the omnibus to the deploy node
- Run migrations on the deploy node
- Deploy to gitaly (apt-get install gitlab-ee and restarts the restarts gitaly)
- Deploy to the rest of the fleet
     - parallel by role, done currently, apt-get install gitlab-ee and restarts the corresponding service
 - Start chef


#### Running

_Note: you must have a ~/.chef directory to populate inventory_

```
pip install -r requirements.txt
export TAKEOFF_ENV=gstg
$ ansible-playbook -i chef_inventory.py deploy.yml

PLAY [Stop chef for all roles] **********************************************************************

TASK [Stop Chef] ************************************************************************************
ok: [10.224.1.4] => {
    "msg": "Stopping chef on 10.224.1.4"
}
ok: [10.224.1.2] => {
    "msg": "Stopping chef on 10.224.1.2"
}
ok: [10.224.1.3] => {
    "msg": "Stopping chef on 10.224.1.3"
}
ok: [10.224.12.3] => {
    "msg": "Stopping chef on 10.224.12.3"
}
ok: [10.224.12.2] => {
    "msg": "Stopping chef on 10.224.12.2"
}
ok: [10.224.12.4] => {
    "msg": "Stopping chef on 10.224.12.4"
}
ok: [10.224.13.4] => {
    "msg": "Stopping chef on 10.224.13.4"
}
ok: [10.224.13.2] => {
    "msg": "Stopping chef on 10.224.13.2"
}
ok: [10.224.13.3] => {
    "msg": "Stopping chef on 10.224.13.3"
}
ok: [10.224.15.2] => {
    "msg": "Stopping chef on 10.224.15.2"
}
ok: [10.224.2.102] => {
    "msg": "Stopping chef on 10.224.2.102"
}
ok: [10.224.2.101] => {
    "msg": "Stopping chef on 10.224.2.101"
}

PLAY [Update the version role in Chef] **************************************************************

TASK [Update the chef role to the new version for gstg] *********************************************
changed: [localhost -> localhost]

PLAY [Run GitLab Migrations] ************************************************************************

TASK [Install package] ******************************************************************************
ok: [10.224.15.2] => {
    "msg": "Installing package on 10.224.15.2"
}

TASK [Run migrations] *******************************************************************************
ok: [10.224.15.2] => {
    "msg": "Running migrations on 10.224.15.2"
}

PLAY [Deploy to Gitaly] *****************************************************************************

TASK [Install package] ******************************************************************************
ok: [10.224.2.102] => {
    "msg": "Installing package on 10.224.2.102"
}
ok: [10.224.2.101] => {
    "msg": "Installing package on 10.224.2.101"
}

PLAY [Deploy to Web Fleet] **************************************************************************

TASK [Drain host from load balancer] ****************************************************************
ok: [10.224.1.4] => {
    "msg": "draining 10.224.1.4"
}
ok: [10.224.1.2] => {
    "msg": "draining 10.224.1.2"
}
ok: [10.224.1.3] => {
    "msg": "draining 10.224.1.3"
}

TASK [Install package] ******************************************************************************
changed: [10.224.1.4] => {
    "msg": "Installing package on 10.224.1.4"
}
changed: [10.224.1.2] => {
    "msg": "Installing package on 10.224.1.2"
}
changed: [10.224.1.3] => {
    "msg": "Installing package on 10.224.1.3"
}

RUNNING HANDLER [HUP Unicorn] ***********************************************************************
ok: [10.224.1.4] => {
    "msg": "Hup'ing unicorn on 10.224.1.4"
}
ok: [10.224.1.2] => {
    "msg": "Hup'ing unicorn on 10.224.1.2"
}
ok: [10.224.1.3] => {
    "msg": "Hup'ing unicorn on 10.224.1.3"
}

TASK [Enabling host on load balancer] ***************************************************************
ok: [10.224.1.4] => {
    "msg": "enabling 10.224.1.4"
}
ok: [10.224.1.2] => {
    "msg": "enabling 10.224.1.2"
}
ok: [10.224.1.3] => {
    "msg": "enabling 10.224.1.3"
}

PLAY [Deploy to Web Fleet] **************************************************************************

TASK [Drain host from load balancer] ****************************************************************
ok: [10.224.12.3] => {
    "msg": "draining 10.224.12.3"
}
ok: [10.224.12.2] => {
    "msg": "draining 10.224.12.2"
}
ok: [10.224.12.4] => {
    "msg": "draining 10.224.12.4"
}

TASK [Install package] ******************************************************************************
changed: [10.224.12.3] => {
    "msg": "Installing package on 10.224.12.3"
}
changed: [10.224.12.2] => {
    "msg": "Installing package on 10.224.12.2"
}
changed: [10.224.12.4] => {
    "msg": "Installing package on 10.224.12.4"
}

RUNNING HANDLER [HUP Unicorn] ***********************************************************************
ok: [10.224.12.3] => {
    "msg": "Hup'ing unicorn on 10.224.12.3"
}
ok: [10.224.12.2] => {
    "msg": "Hup'ing unicorn on 10.224.12.2"
}
ok: [10.224.12.4] => {
    "msg": "Hup'ing unicorn on 10.224.12.4"
}

TASK [Enabling host on load balancer] ***************************************************************
ok: [10.224.12.3] => {
    "msg": "enabling 10.224.12.3"
}
ok: [10.224.12.2] => {
    "msg": "enabling 10.224.12.2"
}
ok: [10.224.12.4] => {
    "msg": "enabling 10.224.12.4"
}

PLAY [Deploy to Web Fleet] **************************************************************************

TASK [Drain host from load balancer] ****************************************************************
ok: [10.224.13.4] => {
    "msg": "draining 10.224.13.4"
}
ok: [10.224.13.2] => {
    "msg": "draining 10.224.13.2"
}
ok: [10.224.13.3] => {
    "msg": "draining 10.224.13.3"
}

TASK [Install package] ******************************************************************************
changed: [10.224.13.4] => {
    "msg": "Installing package on 10.224.13.4"
}
changed: [10.224.13.2] => {
    "msg": "Installing package on 10.224.13.2"
}
changed: [10.224.13.3] => {
    "msg": "Installing package on 10.224.13.3"
}

RUNNING HANDLER [HUP Unicorn] ***********************************************************************
ok: [10.224.13.4] => {
    "msg": "Hup'ing unicorn on 10.224.13.4"
}
ok: [10.224.13.2] => {
    "msg": "Hup'ing unicorn on 10.224.13.2"
}
ok: [10.224.13.3] => {
    "msg": "Hup'ing unicorn on 10.224.13.3"
}

TASK [Enabling host on load balancer] ***************************************************************
ok: [10.224.13.4] => {
    "msg": "enabling 10.224.13.4"
}
ok: [10.224.13.2] => {
    "msg": "enabling 10.224.13.2"
}
ok: [10.224.13.3] => {
    "msg": "enabling 10.224.13.3"
}

PLAY RECAP ******************************************************************************************
10.224.1.2                 : ok=5    changed=1    unreachable=0    failed=0
10.224.1.3                 : ok=5    changed=1    unreachable=0    failed=0
10.224.1.4                 : ok=5    changed=1    unreachable=0    failed=0
10.224.12.2                : ok=5    changed=1    unreachable=0    failed=0
10.224.12.3                : ok=5    changed=1    unreachable=0    failed=0
10.224.12.4                : ok=5    changed=1    unreachable=0    failed=0
10.224.13.2                : ok=5    changed=1    unreachable=0    failed=0
10.224.13.3                : ok=5    changed=1    unreachable=0    failed=0
10.224.13.4                : ok=5    changed=1    unreachable=0    failed=0
10.224.15.2                : ok=3    changed=0    unreachable=0    failed=0
10.224.2.101               : ok=2    changed=0    unreachable=0    failed=0
10.224.2.102               : ok=2    changed=0    unreachable=0    failed=0
localhost                  : ok=1    changed=1    unreachable=0    failed=0

```
